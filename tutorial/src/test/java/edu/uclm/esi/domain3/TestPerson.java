package edu.uclm.esi.domain3;

import static org.junit.Assert.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

import edu.uclm.esi.annotations.Object2JSON;

public class TestPerson {
	private Person juan;
	private Vehicle seat;
	
	@Before
	public void setUp() {
		juan=new Person(); juan.setName("Juan"); juan.setLastNames(new String[] { "Pérez", "López"} ); juan.setAge(20);
		seat=new Vehicle(); seat.setBrand("Seat"); seat.setModel("Panda"); seat.setPlate("1234 ABC");
	}

	@Test
	public void testJuan() {
		JSONObject jsoJuan;
		try {
			jsoJuan = Object2JSON.toJSON(juan);

			System.out.println(jsoJuan);
			assertTrue(jsoJuan.getString("name").equals("Juan"));
			
			JSONArray apellidos=jsoJuan.getJSONArray("lastNames");
			assertTrue(apellidos.get(0).equals("Pérez") && apellidos.get(1).equals("López"));
			
			assertTrue(juan.getAge()==20);
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	public void testSeat() {
		try {
			JSONObject jsoSeat = Object2JSON.toJSON(seat);
			System.out.println(jsoSeat);
			assertTrue(jsoSeat.getString("brand").equals("Seat"));
			assertTrue(jsoSeat.getString("model").equals("Panda"));
			assertTrue(jsoSeat.getString("plate").equals("1234 ABC"));
		}
		catch (Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testJuanYSuSeat() {
		seat.setOwner(juan);
		try {
			JSONObject jsoSeat = Object2JSON.toJSON(seat);
			System.out.println(jsoSeat);
			assertTrue(jsoSeat.getString("brand").equals("Seat"));
			assertTrue(jsoSeat.getString("model").equals("Panda"));
			assertTrue(jsoSeat.getString("plate").equals("1234 ABC"));
			
			JSONObject jsoJuan=jsoSeat.getJSONObject("owner");
			assertTrue(jsoJuan.getString("name").equals("Juan"));
			
			JSONArray apellidos=jsoJuan.getJSONArray("lastNames");
			assertTrue(apellidos.get(0).equals("Pérez") && apellidos.get(1).equals("López"));
			
			assertTrue(juan.getAge()==20);
		}
		catch(Exception e) {
			fail(e.getMessage());
		}
	}
}

package edu.uclm.esi.domain3;

import java.lang.reflect.Field;

import org.json.JSONObject;

import edu.uclm.esi.annotations.JSONable;

public class Deserializador {

	public static void main(String[] args) throws Exception {
		JSONObject jsoAna=new JSONObject().put("name", "Ana")
				.put("lastNames", new String[] {"López", "Martínez"})
				.put("age", 40);
		
		Person ana=(Person) toObject(Person.class, jsoAna);
		System.out.println(ana.getName());
		System.out.println(ana.getLastNames()[0]);
		System.out.println(ana.getLastNames()[1]);
		System.out.println(ana.getAge());
	}

	private static Object toObject(Class clazz, JSONObject jso) throws Exception {
		Object result=clazz.newInstance();
		Field[] campos=clazz.getDeclaredFields();
		for (Field campo : campos) {
			if (campo.isAnnotationPresent(JSONable.class)) {
				String nombreCampo=campo.getName();
				Object valor=jso.get(nombreCampo);
				campo.setAccessible(true);
				campo.set(result, valor); 
			}
		}
		return result;
	}

}

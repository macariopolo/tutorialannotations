package edu.uclm.esi.domain3;

import java.lang.reflect.Field;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.annotations.JSONable;

public class Reflector {

	public static void main(String[] args) throws Exception {
		//Class clase=new Person().getClass();
		Class clasePerson=Class.forName("edu.uclm.esi.domain3.Person");
		Field[] campos=clasePerson.getDeclaredFields();
		for (Field campo : campos) {
			if (campo.isAnnotationPresent(JSONable.class)) 
				System.out.println(campo.getName() + " es JSONable");
		}
		
		Person ana=new Person();
		ana.setName("Ana"); 
		ana.setLastNames(new String[] { "Pérez", "López"} );
		ana.setAge(25);
		
		JSONObject jsoAna=toJSON(ana);
		System.out.println(jsoAna);
		
		Vehicle seat=new Vehicle(); seat.setBrand("Seat");
		seat.setModel("Panda"); seat.setPlate("1234 ABC");
		seat.setOwner(ana);
		JSONObject jsoSeat=toJSON(seat);
		System.out.println(jsoSeat);
	}

	private static JSONObject toJSON(Object objeto) throws Exception {
		JSONObject result=new JSONObject();
		for (Field campo : objeto.getClass().getDeclaredFields()) {
			campo.setAccessible(true);
			Object valor=campo.get(objeto);
			if (campo.isAnnotationPresent(JSONable.class)) {
				if (campo.getType().isPrimitive() || campo.getType()==String.class)
					result.put(campo.getName(), valor);
				else if (campo.getType().isArray()) {
					Object[] array=(Object[]) valor;
					JSONArray jsa=new JSONArray();
					for (int i=0; i<array.length; i++)
						jsa.put(array[i]);
					result.put(campo.getName(), jsa);
				} else 
					result.put(campo.getName(), toJSON(valor));
			}
		}
		return result;
	}

}

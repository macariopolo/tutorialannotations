package edu.uclm.esi.domain4;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.uclm.esi.annotations.JSONable;

public class Deserializador {

	public static void main(String[] args) throws Exception {
		JSONObject jsoAna=new JSONObject().put("name", "Ana")
				.put("lastNames", new JSONArray().put("López").put("Martínez"))
				.put("age", 40);
		JSONObject jsoSeat=new JSONObject().put("plate", "1234 ABC").
				put("brand", "Seat").put("model", "Panda");
		jsoAna.put("vehicles", new JSONArray().put(jsoSeat));
		
		Person ana=(Person) toObject(Person.class, jsoAna);
		System.out.println(ana.getName());
		System.out.println(ana.getAge());
		System.out.println(ana.getLastNames().length);
		System.out.println(ana.getVehicles().size());
		System.out.println(ana.getVehicles().get(0).getPlate());
		System.out.println(ana.getVehicles().get(0).getBrand());
		System.out.println(ana.getVehicles().get(0).getModel());
	}

	private static Object toObject(Class clazz, JSONObject jso) throws Exception {
		Object result=clazz.newInstance();
		Field[] campos=clazz.getDeclaredFields();
		for (Field campo : campos) {
			if (campo.isAnnotationPresent(JSONable.class)) {
				String nombreCampo=campo.getName();
				Object valor=jso.get(nombreCampo);
				campo.setAccessible(true);
				if (campo.getType().isPrimitive() || campo.getType()==String.class)
					campo.set(result, valor);
				else if (Collection.class.isAssignableFrom(campo.getType())) {
					Collection coleccionVacia=(Collection) campo.getType().newInstance();
					String tipoElementoContenido=campo.getGenericType().getTypeName();
					tipoElementoContenido=tipoElementoContenido.substring(
							tipoElementoContenido.indexOf("<")+1, tipoElementoContenido.indexOf(">"));
					Class claseContenido=Class.forName(tipoElementoContenido);
					JSONArray jsa=jso.getJSONArray(nombreCampo);
					for (int i=0; i<jsa.length(); i++) {
						JSONObject elementoContenido=jsa.getJSONObject(i);
						Object elementito=toObject(claseContenido, elementoContenido);
						coleccionVacia.add(elementito);
					}
					campo.set(result, coleccionVacia);
				} else if (campo.getType().isArray()) {
					String tipoElementoContenido=campo.getGenericType().getTypeName();
					tipoElementoContenido=tipoElementoContenido.substring(
							tipoElementoContenido.indexOf("<")+1, tipoElementoContenido.indexOf(">"));
					Class claseContenido=Class.forName(tipoElementoContenido);
					JSONArray jsa=jso.getJSONArray(nombreCampo);
					campo.set(result, Array.newInstance(claseContenido, jsa.length()));
					// result.lastNames = new String[2];
				}
			}
		}
		return result;
	}

}

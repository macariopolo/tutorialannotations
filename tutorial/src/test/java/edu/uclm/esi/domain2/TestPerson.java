package edu.uclm.esi.domain2;

import static org.junit.Assert.*;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;

public class TestPerson {
	private Person juan;
	private Vehicle seat;
	
	@Before
	public void setUp() {
		juan=new Person(); juan.setName("Juan"); juan.setLastNames(new String[] { "Pérez", "López"} ); juan.setAge(20);
		seat=new Vehicle(); seat.setBrand("Seat"); seat.setModel("Panda"); seat.setPlate("1234 ABC");
	}

	@Test
	public void testJuan() {
		JSONObject jsoJuan = juan.toJSONObject();
		System.out.println(jsoJuan);
		assertTrue(jsoJuan.getString("name").equals("Juan"));
		
		JSONArray apellidos=jsoJuan.getJSONArray("lastNames");
		assertTrue(apellidos.get(0).equals("Pérez") && apellidos.get(1).equals("López"));
		
		assertTrue(juan.getAge()==20);
	}

	@Test
	public void testSeat() {
		JSONObject jsoSeat = seat.toJSONObject();
		System.out.println(jsoSeat);
		assertTrue(jsoSeat.getString("brand").equals("Seat"));
		assertTrue(jsoSeat.getString("model").equals("Panda"));
		assertTrue(jsoSeat.getString("plate").equals("1234 ABC"));
	}
	
	@Test
	public void testJuanYSuSeat() {
		juan.addVehicle(seat);
		JSONObject jsoJuan=juan.toJSONObject();
		System.out.println(jsoJuan);
		
		JSONArray jsaVehicles=jsoJuan.getJSONArray("vehicles");
		JSONObject jsoSeat=jsaVehicles.getJSONObject(0);
		System.out.println(jsoSeat);
		assertTrue(jsoSeat.getString("brand").equals("Seat"));
		assertTrue(jsoSeat.getString("model").equals("Panda"));
		assertTrue(jsoSeat.getString("plate").equals("1234 ABC"));
	}
}

package edu.uclm.esi.domain1;

import java.lang.reflect.Field;

public class Reflector {

	public static void main(String[] args) throws Exception {
		Person ana=new Person();
		ana.setName("Ana");
		ana.setLastNames(new String[] { "García", "García"});
		ana.setAge(30);

		Class clase=ana.getClass();
		System.out.println(clase.getName());
		Field[] campos=clase.getDeclaredFields();
		System.out.println(campos.length);
		
		//System.out.println(ana.getName());
		Field campoName=campos[0];
		System.out.println(campoName.getName());
		campoName.setAccessible(true);
		Object valor = campoName.get(ana); // ana.getName()
		System.out.println(valor);
		
		campoName.set(ana, "Ana María"); // ana.setName("Ana María");
		System.out.println(ana.getName());
		
		Person juan=(Person) clase.newInstance();  // juan=new Person();
		campoName.set(juan, "Juan");
		Field campoLastNames=clase.getDeclaredField("lastNames");
		Field campoAge=clase.getDeclaredField("age");
		campoLastNames.setAccessible(true);
		campoLastNames.set(juan, new String[] { "Pérez", "Pérez"} );
		campoAge.setAccessible(true);
		campoAge.set(juan, 25);
		System.out.println(juan.toJSONObject());
	}

}

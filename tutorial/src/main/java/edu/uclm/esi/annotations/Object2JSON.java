package edu.uclm.esi.annotations;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

public class Object2JSON { 

	public static JSONObject toJSON(Object object) throws Exception {
		if (object==null)
			return new JSONObject();
		Class<?> clazz=object.getClass();
		Vector<Field> fields=JSONableCommons.getJSONableFields(clazz);
		if (fields.size()==0)
			throw new Exception("The object class (" + object.getClass().getName() + ") has no JSONable fields");
		
		JSONObject result=new JSONObject();
		Class<?> fieldType;
		for (Field field : fields) {
			fieldType=field.getType();
			field.setAccessible(true);
			if (fieldType.isPrimitive() || fieldType==String.class || fieldType==Boolean.class || Number.class.isAssignableFrom(fieldType)) {
				result.put(field.getName(), getValue(field, object));
			} else if (Number.class.isAssignableFrom(fieldType)) {
				Field valueField=fieldType.getDeclaredField("value");
				valueField.setAccessible(true);
				result.put(field.getName(), valueField.get(object));
			} else
				result.put(field.getName(), getValue(field, object));
		}
		return result;
	}
	
	private static Object getValue(Field field, Object object) throws Exception {
		field.setAccessible(true);
		Class<?> fieldType=field.getType();
		if (fieldType.isPrimitive() || fieldType==String.class || fieldType==Boolean.class || Number.class.isAssignableFrom(fieldType)) {
			Object fieldValue=field.get(object);
			return fieldValue; 
		} else if (fieldType.isArray()) {
			Class<?> containedType=fieldType.getComponentType();
			if (containedType.isPrimitive()) {
				JSONArray jsa=getJSONArrayOfPrimitive(field, object, containedType);
				return jsa;
			} else if (containedType==String.class || containedType==Boolean.class || Number.class.isAssignableFrom(containedType)) {
				JSONArray jsa=getJSONArrayOfWrapperType(field, object, containedType);
				return jsa;
			} else {
				JSONArray jsa=getJSONArrayOfGenericObject(field, object);
				return jsa;
			}
		} else if (Collection.class.isAssignableFrom(fieldType)) {
			Collection<?> collection=(Collection<?>) field.get(object);
			if (collection==null)
				return null;
			JSONArray jsa=new JSONArray();
			for (Object containedObject : collection)
				if (containedObject!=null)
					jsa.put(toJSON(containedObject));
			return jsa;
		}
		return toJSON(field.get(object));
	}
	
	
	private static JSONArray getJSONArrayOfGenericObject(Field field, Object object) throws Exception {
		JSONArray jsa=new JSONArray(); 		
		Object[] array=(Object[]) field.get(object);
		Object containedElement;
		JSONObject jsoElement;
		for (int i=0; i<array.length; i++) {
			containedElement=array[i];
			if (containedElement!=null) {
				jsoElement=toJSON(containedElement);
				jsa.put(jsoElement);
			}
		}
		return jsa;
	}

	private static JSONArray getJSONArrayOfWrapperType(Field field, Object object, Class<?> containedType) throws Exception {
		JSONArray jsa=new JSONArray(); 		
		Object[] array=(Object[]) field.get(object);
		for (int i=0; i<array.length; i++)
			jsa.put(array[i]);
		return jsa;
	}

	private static JSONArray getJSONArrayOfPrimitive(Field field, Object object, Class<?> containedType) throws Exception {
		JSONArray jsa=new JSONArray(); 
		if (containedType==boolean.class) {
			boolean[] array=(boolean[]) field.get(object);
			for (int i=0; i<array.length; i++)
				jsa.put(array[i]);
		} else if (containedType==char.class) {
			char[] array=(char[]) field.get(object);
			for (int i=0; i<array.length; i++)
				jsa.put(array[i]);
		} else if (containedType==byte.class) {
			byte[] array=(byte[]) field.get(object);
			for (int i=0; i<array.length; i++)
				jsa.put(array[i]);
		} else if (containedType==int.class) {
			int[] array=(int[]) field.get(object);
			for (int i=0; i<array.length; i++)
				jsa.put(array[i]);
		} else if (containedType==long.class) {
			long[] array=(long[]) field.get(object);
			for (int i=0; i<array.length; i++)
				jsa.put(array[i]);
		} else if (containedType==float.class) {
			float[] array=(float[]) field.get(object);
			for (int i=0; i<array.length; i++)
				jsa.put(array[i]);
		} else if (containedType==double.class) {
			double[] array=(double[]) field.get(object);
			for (int i=0; i<array.length; i++)
				jsa.put(array[i]);
		}
		return jsa;
	}
}

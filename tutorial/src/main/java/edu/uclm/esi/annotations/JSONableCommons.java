package edu.uclm.esi.annotations;

import java.lang.reflect.Field;
import java.util.Vector;

public class JSONableCommons {
	static Vector<Field> getJSONableFields(Class<?> clazz) {
		Vector<Field> result=new Vector<>();
		Field[] fields=clazz.getDeclaredFields();
		Field field;
		for (int i=0; i<fields.length; i++) {
			field=fields[i];
			if (isJSONable(field))
				result.add(field);
		}
		return result;
	}

	private static boolean isJSONable(Field field) {
		return (field.getAnnotation(JSONable.class)!=null) || isJSONable(field.getType()); 
	}

	public static boolean isJSONable(Class<?> clazz) {
		if (clazz.isPrimitive() || clazz==String.class || Number.class.isAssignableFrom(clazz) || clazz==Boolean.class) 
			return true;
		
		Field[] fields=clazz.getDeclaredFields();
		Field field;
		for (int i=0; i<fields.length; i++) {
			field=fields[i];
			if (isJSONable(field))
				return true;
		}			
		return false;
	}
}

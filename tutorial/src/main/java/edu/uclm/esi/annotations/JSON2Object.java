package edu.uclm.esi.annotations;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSON2Object {
	
	public static Object toObject(Class<?> clazz, JSONObject jso) throws Exception {
		Vector<Field> fields=JSONableCommons.getJSONableFields(clazz);
		if (fields.size()==0)
			throw new Exception("The object class (" + clazz.getName() + ") has no JSONable fields");
		
		Object object;
		try {
			object=clazz.newInstance();
		}
		catch (Exception e) {
			throw new Exception("The class " + clazz.getName() + " requires a public constructor with no parameters");
		}
		
		String fieldName;
		Object fieldValue;
		for (Field field : fields) {
			field.setAccessible(true);
			fieldName=field.getName();
			fieldValue=jso.opt(fieldName);
			if (fieldValue==null)
				continue;
			set(object, field, fieldValue);
		}
		return object;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void set(Object object, Field field, Object fieldValue) throws Exception {
		Class<?> fieldType=(Class<?>) field.getType();
		if (fieldType.isPrimitive() || fieldType==String.class || Number.class.isAssignableFrom(fieldType))
			field.set(object, fieldValue);
		else if (fieldType.isArray()) {
			JSONArray jsa=(JSONArray) fieldValue;
			Class<?> containedType=fieldType.getComponentType();
			if (containedType.isPrimitive() || containedType==String.class)
				field.set(object, getArrayOfPrimitives(containedType, jsa));
			else if (Number.class.isAssignableFrom(containedType)) 
				field.set(object, getArrayOfWrapperType(containedType, jsa));
			else if (JSONableCommons.isJSONable(containedType)) {
				Object[] array=getArrayOfJSONables(containedType, jsa);
				field.set(object, Array.newInstance(containedType, array.length));
				for (int i=0; i<array.length; i++) {
					Array.set(field.get(object), i, array[i]);
				}
			}
		} else if (Collection.class.isAssignableFrom(fieldType)) {
			String fieldSignature=field.getGenericType().toString();
			String containedTypeName=fieldSignature.substring(fieldSignature.indexOf("<")+1, fieldSignature.length()-1);
			Class<?> containedType=(Class<?>) Class.forName(containedTypeName);
			Collection collection=(Collection) fieldType.newInstance();
			JSONArray jsa=(JSONArray) fieldValue;
			if (containedType==String.class) {
				String containedObject;
				for (int i=0; i<jsa.length(); i++) {
					containedObject=jsa.getString(i);
					collection.add(containedObject);
				}
			} else if (Number.class.isAssignableFrom(containedType)) {
				field.set(object, getArrayOfWrapperType(containedType, jsa));
			} else if (JSONableCommons.isJSONable(containedType)) {
				Object containedObject;
				JSONObject jsoContainedObject;
				for (int i=0; i<jsa.length(); i++) {
					jsoContainedObject=jsa.getJSONObject(i);
					containedObject=toObject(containedType, jsoContainedObject);
					collection.add(containedObject);
				}
			}			
			field.set(object, collection);
		} else if (JSONableCommons.isJSONable(fieldType)) {
			JSONObject jsoFieldValue=(JSONObject) fieldValue;
			field.set(object, toObject(fieldType, jsoFieldValue));
		}
	}

	private static Object[] getArrayOfJSONables(Class<?> containedType, JSONArray jsa) throws Exception {
		Object[] array=new Object[jsa.length()];
		JSONObject jsoContainedObject;
		Object containedObject;
		for (int i=0; i<jsa.length(); i++) {
			jsoContainedObject=jsa.getJSONObject(i);
			containedObject=toObject(containedType, jsoContainedObject);
			array[i]=containedObject;
		}
		return array;
	}

	private static Object getArrayOfWrapperType(Class<?> containedType, JSONArray jsa) throws Exception {
		if (containedType==Boolean.class) {
			Boolean[] array=new Boolean[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=new Boolean(jsa.optBoolean(i));
			return array;
		}
		if (containedType==Character.class) {
			Character[] array=new Character[jsa.length()];
			String value;
			for (int i=0; i<jsa.length(); i++) {
				value=jsa.optString(i);
				if (value.length()>0)
					array[i]=new Character(value.charAt(0));
			}
			return array;
		}
		if (containedType==Byte.class) {
			Byte[] array=new Byte[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=new Byte((byte) jsa.optInt(i));
			return array;
		}
		if (containedType==Short.class) {
			Short[] array=new Short[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=new Short((short) jsa.optInt(i));
			return array;
		}
		if (containedType==Integer.class) {
			Integer[] array=new Integer[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=new Integer(jsa.optInt(i));
			return array;
		}
		if (containedType==Long.class) {
			Long[] array=new Long[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=new Long(jsa.optLong(i));
			return array;
		}
		if (containedType==Float.class) {
			Float[] array=new Float[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=new Float(jsa.optDouble(i));
			return array;
		}
		if (containedType==Double.class) {
			Double[] array=new Double[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=new Double(jsa.optDouble(i));
			return array;
		}
		throw new Exception("Unrecognized wrapper data type: " + containedType.getName());
	}

	private static Object getArrayOfPrimitives(Class<?> containedType, JSONArray jsa) throws Exception {
		if (containedType==boolean.class) {
			boolean[] array=new boolean[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=jsa.getBoolean(i);
			return array;
		} 
		if (containedType==char.class) {
			char[] array=new char[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=jsa.getString(i).charAt(0);
			return array;
		}
		if (containedType==byte.class) {
			byte[] array=new byte[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=(byte) jsa.getInt(i);
			return array;
		}
		if (containedType==short.class) {
			short[] array=new short[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=(short) jsa.getInt(i);
			return array;
		}
		if (containedType==int.class) {
			int[] array=new int[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=jsa.getInt(i);
			return array;
		}
		if (containedType==long.class) {
			long[] array=new long[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=jsa.getLong(i);
			return array;
		}
		if (containedType==float.class) {
			float[] array=new float[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=(float) jsa.getDouble(i);
			return array;
		}
		if (containedType==double.class) {
			double[] array=new double[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=jsa.getDouble(i);
			return array;
		}
		if (containedType==String.class) {
			String[] array=new String[jsa.length()];
			for (int i=0; i<jsa.length(); i++)
				array[i]=jsa.optString(i);
			return array;
		} 
		throw new Exception("Unrecognized primtive data type: " + containedType.getName());
	}
}

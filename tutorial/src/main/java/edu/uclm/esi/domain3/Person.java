package edu.uclm.esi.domain3;

import edu.uclm.esi.annotations.JSONable;

public class Person {
	@JSONable
	private String name;
	@JSONable
	private String[] lastNames;
	@JSONable
	private int age;
	
	public Person() {
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getLastNames() {
		return lastNames;
	}

	public void setLastNames(String[] lastNames) {
		this.lastNames = lastNames;
	}
		
	public void setAge(int age) {
		this.age=age;
	}
	
	public int getAge() {
		return age;
	}
}

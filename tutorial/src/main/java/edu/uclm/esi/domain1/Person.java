package edu.uclm.esi.domain1;

import org.json.JSONArray;
import org.json.JSONObject;

public class Person {
	private String name;
	private String[] lastNames;
	private int age;
	
	public Person() {
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getLastNames() {
		return lastNames;
	}

	public void setLastNames(String[] lastNames) {
		this.lastNames = lastNames;
	}
		
	public void setAge(int age) {
		this.age=age;
	}
	
	public int getAge() {
		return age;
	}
	
	public JSONObject toJSONObject() {
		return new JSONObject().
				put("name", this.name).
				put("lastNames", new JSONArray().put(lastNames[0]).put(lastNames[1])).
				put("age", this.age);
	}
}

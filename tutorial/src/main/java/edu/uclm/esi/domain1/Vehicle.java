package edu.uclm.esi.domain1;

import org.json.JSONObject;

public class Vehicle {
	private String plate;
	private String brand;
	private String model;
	private Person owner;

	public Vehicle() {
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public Person getOwner() {
		return owner;
	}

	public void setOwner(Person owner) {
		this.owner = owner;
	}
	
	public JSONObject toJSONObject() {
		JSONObject jso=new JSONObject().put("plate", plate).put("brand", brand).put("model", model);
		if (owner!=null)
			jso.put("owner", owner.toJSONObject());
		return jso;
	}
}

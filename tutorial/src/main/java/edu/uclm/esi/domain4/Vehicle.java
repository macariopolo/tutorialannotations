package edu.uclm.esi.domain4;

import edu.uclm.esi.annotations.JSONable;

public class Vehicle {
	@JSONable
	private String plate;
	@JSONable
	private String brand;
	@JSONable
	private String model;

	public Vehicle() {
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
}

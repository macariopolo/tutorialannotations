package edu.uclm.esi.domain4;

import java.util.Vector;

import edu.uclm.esi.annotations.JSONable;

public class Person {
	@JSONable
	private String name;
	@JSONable
	private String[] lastNames;
	@JSONable
	private int age;
	@JSONable
	private Vector<Vehicle> vehicles;
	
	public Person() {
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getLastNames() {
		return lastNames;
	}

	public void setLastNames(String[] lastNames) {
		this.lastNames = lastNames;
	}
		
	public void setAge(int age) {
		this.age=age;
	}
	
	public int getAge() {
		return age;
	}
	
	public void addVehicle(Vehicle vehicle) {
		if (vehicles==null)
			vehicles=new Vector<>();
		vehicles.add(vehicle);
	}
	
	public Vector<Vehicle> getVehicles() {
		return vehicles;
	}
}

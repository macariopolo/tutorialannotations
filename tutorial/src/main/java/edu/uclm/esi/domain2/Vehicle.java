package edu.uclm.esi.domain2;

import org.json.JSONObject;

public class Vehicle {
	private String plate;
	private String brand;
	private String model;

	public Vehicle() {
	}

	public String getPlate() {
		return plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}
	
	public JSONObject toJSONObject() {
		return new JSONObject().put("plate", plate).put("brand", brand).put("model", model);
	}
}

package edu.uclm.esi.domain2;

import java.util.Vector;

import org.json.JSONArray;
import org.json.JSONObject;

public class Person {
	private String name;
	private String[] lastNames;
	private int age;
	private Vector<Vehicle> vehicles;
	
	public Person() {
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String[] getLastNames() {
		return lastNames;
	}

	public void setLastNames(String[] lastNames) {
		this.lastNames = lastNames;
	}
		
	public void setAge(int age) {
		this.age=age;
	}
	
	public int getAge() {
		return age;
	}
	
	public void addVehicle(Vehicle vehicle) {
		if (vehicles==null)
			vehicles=new Vector<>();
		vehicles.add(vehicle);
	}
	
	public Vector<Vehicle> getVehicles() {
		return vehicles;
	}
	
	public JSONObject toJSONObject() {
		JSONObject jso = new JSONObject().
				put("name", this.name).
				put("lastNames", new JSONArray().put(lastNames[0]).put(lastNames[1])).
				put("age", this.age);
		if (vehicles!=null) {
			JSONArray jsaVehicles=new JSONArray();
			for (Vehicle vehicle : vehicles)
				jsaVehicles.put(vehicle.toJSONObject());
			jso.put("vehicles", jsaVehicles);
		}
		return jso;
	}
}
